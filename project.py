#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Project(ModelSQL, ModelView):
    'Project'
    __name__ = 'property.project'
    name = fields.Char('Project Name', required=True)
    code = fields.Char('Code')
    description = fields.Char('Description')

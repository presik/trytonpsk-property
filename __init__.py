#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .property_study import (PropertyStudy, Boundary, TraditionAttribute,
    AnnotationLegalTradition, PropertyUse, PropertyTableReport,
    PropertyStudyReport, PropertyOwner, LegalTraditionFromParty,
    LegalTraditionToParty, PropertyOwnerLegal, PropertyOwnerBoundary)
from .property_type import PropertyType
from .configuration import Configuration
from .project import Project


def register():
    Pool.register(
        Configuration,
        Project,
        PropertyType,
        PropertyUse,
        TraditionAttribute,
        PropertyStudy,
        Boundary,
        AnnotationLegalTradition,
        PropertyOwner,
        PropertyOwnerLegal,
        PropertyOwnerBoundary,
        LegalTraditionFromParty,
        LegalTraditionToParty,
        module='property', type_='model')
    Pool.register(
        PropertyStudyReport,
        PropertyTableReport,
        module='property', type_='report')

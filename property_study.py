# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval
from trytond.report import Report
from trytond.pool import Pool
from trytond.transaction import Transaction

_STATES_PROPERTY = states = {
    'invisible': Eval('type_boundary') != 'property_land',
}

YES_NO_SEL = [
            ('yes', 'Yes'),
            ('no', 'No'),
        ]

RANGE_STRATUM = [(str(i+1), str(i+1)) for i in range(6)]

RANGE_STRATUM.extend([('na', 'N.A.')])


class PropertyStudy(ModelSQL, ModelView):
    """Property Study"""

    __name__ = 'property.study'
    _rec_name = 'code'
    code = fields.Char("Code", select=True, required=True)
    name = fields.Char("Name/Address", select=True, required=True)
    type_property = fields.Many2One('property.type',
        'Property Type', required=True)
    cadastral_register = fields.Char("Cadastral Register", select=True)
    registration_number = fields.Char("Registration Number",
        select=True)
    state = fields.Selection([
        ('rental', 'Rental'),
        ('commodatum', 'Commodatum'),
        ('party_occupied', 'Party Occupied'),
        ('invade', 'Invade'),
        ('available_unoccupied', 'Available and Unoccupied'),
        ('own_use', 'Own Use'),
        ('public_use', 'Public Use'),
        ('national_park', 'National Park'),
        ('easement', 'Easement'),
        ], 'State', select=True)
    state_string = state.translated('state')
    fee_month = fields.Numeric('Fee Month', states={
            'invisible': Eval('state') != 'rental',
            })
    state_kind_occupation = fields.Selection([
            ('private', 'Private'),
            ('public', 'Public'),
            ('mixed', 'Mixed'),
        ], 'Kind Occupation', select=True, states={
            'invisible': ~Eval('state').in_(['rental', 'commodatum', 'party_occupied', 'invade']),
        })
    state_kind_occupation_string = state_kind_occupation.translated('state_kind_occupation')
    state_document = fields.Selection(YES_NO_SEL, 'State Document',
        required=True,  states={
            'invisible': ~Eval('state').in_(['rental', 'commodatum', 'party_occupied', 'invade']),
        })
    state_document_string = state_document.translated('state_document')
    commercial_appraisal = fields.Numeric("Commercial Appraisal",
        digits=(16,2), select=True)
    assessed_value = fields.Numeric("Assessed Value", select=True,
        digits=(16,2))
    owners = fields.One2Many('property.owner', 'property_study',
        "Owners")
    classification = fields.Selection([
        ('private', 'Private'),
        ('public', 'Public'),
        ], 'Classification', select=True)
    classification_string = classification.translated('classification')
    classification_use = fields.Selection([
            ('', ''),
            ('fiscal_use', 'Fiscal Use'),
            ('public_use', 'Public Use'),
        ], 'Use', select=True, states={
            'invisible': Eval('classification') == 'private',
        })
    classification_use_string = classification_use.translated('classification_use')
    country = fields.Many2One('country.country', 'Country')
    project = fields.Many2One('property.project', 'Project')
    subdivision = fields.Many2One('country.subdivision', 'Subdivision',
        domain=[('country', '=', Eval('country'))],
        depends=['active', 'country']
    )
    city = fields.Char('City')
    rural_settlement = fields.Char("Rural Settlement", select=True)
    neighborhood = fields.Char('Neighborhood')
    social_stratum = fields.Selection(RANGE_STRATUM, 'Social Stratum',
            select=True)
    category = fields.Selection([
            ('rural', 'Rural'),
            ('urban', 'Urban'),
            ], 'Category', select=True, required=True)
    category_string = category.translated('category')
    kind = fields.Selection([
            ('condominium', 'Condominium'),
            ('land', 'Land'),
            ('land_building', 'Land and Building'),
            ], 'Kind', required=True)
    kind_string = kind.translated('kind')
    has_affectations = fields.Selection(YES_NO_SEL, 'Has Affectations',
            required=True)
    has_affectations_string = has_affectations.translated('has_affectations')
    which_affectations = fields.Text('Which Affectations', states={
                'invisible': Eval('has_affectations') == 'no',
        })
    earthquake_resistant = fields.Selection(YES_NO_SEL,
            'Earthquake Resistant', required=True)
    earthquake_resistant_string = earthquake_resistant.translated('earthquake_resistant')
    insured = fields.Selection(YES_NO_SEL, 'Insured', required=True)
    insured_string = insured.translated('insured')
    owners_legal = fields.One2Many('property.owner_legal', 'property_study',
            "Legal Owners")
    land_area = fields.Char("Land Area", select=True)
    land_area_title = fields.Char("Land Area According Title", select=True)
    developed_area = fields.Char("Developed Area", select=True)
    use = fields.Many2One('property.use', 'Property Use')
    property_tax_state = fields.Selection([
            ('up_to_date', 'Up to Date'),
            ('debt', 'Debt'),
            ('aggresive_collection', 'Aggresive Collection'),
            ('exempt', 'Exempt'),
        ], 'Property Tax State', select=True, required=True)
    property_tax_state_string = property_tax_state.translated('property_tax_state')
    cadastral_letter = fields.Text('Cadastral Letter')
    aqueduct = fields.Selection(YES_NO_SEL, 'Aqueduct', required=True)
    aqueduct_string = aqueduct.translated('aqueduct')
    sewage_system = fields.Selection(YES_NO_SEL, 'Sewage System',
            required=True)
    sewage_system_string = sewage_system.translated('sewage_system')
    electric_energy = fields.Selection(YES_NO_SEL, 'Electric Energy',
            required=True)
    electric_energy_string = electric_energy.translated('electric_energy')
    gas_home = fields.Selection(YES_NO_SEL, 'Gas Home', required=True)
    gas_home_string = gas_home.translated('gas_home')
    phone = fields.Selection(YES_NO_SEL, 'Phone', required=True)
    phone_string = phone.translated('phone')
    other_public_utility = fields.Selection(YES_NO_SEL, 'Other Utility',
            required=True)
    other_public_utility_string = other_public_utility.translated('other_public_utility')
    other_public_utility_note = fields.Char('Other Utility Note',
            states={
                'invisible': Eval('other_public_utility', False) == False,
    })
    registration_date = fields.Date('Registration Date')
    adquisition_mode = fields.Selection([
        ('tradition', 'Tradition'),
        ('occupation', 'Occupation'),
        ('succession', 'Succession'),
        ('prescription', 'Prescription'),
        ('accession', 'Accession'),
        ('constitucion_propiedad_horizontal', 'Constitucion Propiedad Horizontal'),
        ('adjudicacion_baldios', 'Adjudicacion de Baldios'),
        ('division_material', 'Division Material'),
        ('Englobe', 'englobe'),
        ('otro', 'Otro'),
        ], 'Adquisition Mode', select=True, required=True)
    adquisition_mode_which = fields.Char('Which Mode', states={
        'invisible': Eval('adquisition_mode') != 'otro',
    })
    adquisition_mode_string = adquisition_mode.translated('adquisition_mode')
    property_history = fields.Text('Property History')
    description = fields.Text('Description')
    measurement_boundary = fields.Text('Measure Boundary')
    boundary_north = fields.Char('Boundary North')
    boundary_orient = fields.Char('Boundary Orient')
    boundary_south = fields.Char('Boundary South')
    boundary_west = fields.Char('Boundary West')
    boundary_nadir = fields.Char('Boundary Nadir')
    boundary_cenit = fields.Char('Boundary Cenit')
    boundaries = fields.One2Many('property.boundary', 'property_study',
        'Boundaries')
    annotation_traditions = fields.One2Many('property.annotation_legal_tradition',
        'property_study', 'Annotation Legal Tradition')
    party = fields.Many2One('party.party', 'Party')
    note_owner = fields.Char('Note Owner')
    note_area = fields.Text('Note Area')
    note_occupation_situation = fields.Text('Note Occupation Situation')
    note_tradition_certificate = fields.Text('Note Tradition Certificate')
    note_others = fields.Text('Note Others')

    @staticmethod
    def default_classification():
        return 'private'

    @staticmethod
    def default_kind():
        return 'land_building'

    @staticmethod
    def default_insured():
        return 'no'

    @staticmethod
    def default_aqueduct():
        return 'no'

    @staticmethod
    def default_sewage_system():
        return 'no'

    @staticmethod
    def default_phone():
        return 'no'

    @staticmethod
    def default_gas_home():
        return 'no'

    @staticmethod
    def default_has_affectations():
        return 'no'

    @staticmethod
    def default_electric_energy_string():
        return 'no'

    @staticmethod
    def default_earthquake_resistant():
        return 'no'

    @staticmethod
    def default_other_public_utility():
        return 'no'

    @staticmethod
    def default_state_document():
        return 'no'

    @staticmethod
    def default_assessed_value():
        return Decimal(0)

    @staticmethod
    def default_commercial_appraisal():
        return Decimal(0)

    @classmethod
    def __setup__(cls):
        super(PropertyStudy, cls).__setup__()
        cls._order = [
            ('code', 'ASC'),
            ('id', 'DESC'),
        ]
        cls._buttons.update({
                'create_owners': {
                    'invisible': False,
                    },
                })


    @classmethod
    @ModelView.button
    def create_owners(cls, records):
        PropertyOwner = Pool().get('property.owner_boundary')
        names = [
            "Municipio de Barrancabermeja - 91.25%",
            "EDUBA – Empresa de Desarrollo Urbano y Vivienda de Interes Social de Barrancabermeja - 8.75 %"
        ]
        for record in records:
            for boundary in record.boundaries:
                if boundary.type_boundary == 'property_land' and not boundary.owners and int(record.code) <= 897:
                    for name in names:
                        PropertyOwner.create([{
                            'boundary': boundary.id,
                            'name': name,
                        }])


class Boundary(ModelSQL, ModelView):
    'Boundary'
    __name__ = 'property.boundary'
    _rec_name = 'sequence'
    property_study = fields.Many2One('property.study', 'Property Study', required=True)
    sequence = fields.Char('Sequence')
    cardinal_point = fields.Selection([
            ('north', 'North'),
            ('orient', 'Orient'),
            ('west', 'West'),
            ('south', 'South'),
            ('nadir', 'Nadir'),
            ('cenit', 'Cenit'),
            ('other', 'Other'),
        ], 'Cardinal Point', required=True, select=True)
    cardinal_point_string = cardinal_point.translated('cardinal_point')
    type_boundary = fields.Selection([
            ('road', 'Road'),
            ('property_land', 'Property Land'),
            ('road_property', 'Road and property'),
            ('other', 'Other'),
        ], 'Type Boundary', select=True, required=True)
    type_boundary_string = type_boundary.translated('type_boundary')
    other_description = fields.Text("Others Description", select=True,
            states={
                'invisible': Eval('type_boundary') != 'other',
    })
    road = fields.Char('Road', states={
            'invisible': Eval('type_boundary') != 'road',
        })
    property_code = fields.Char('Property Code')
    registration_number = fields.Char("Registration Number",
            states=_STATES_PROPERTY)
    address = fields.Char("Name/Address", select=True,
            states=_STATES_PROPERTY)
    neighborhood = fields.Char('Neighborhood', states=_STATES_PROPERTY)
    rural_settlement = fields.Char("Rural Settlement", states=_STATES_PROPERTY)
    municipality = fields.Char("Municipality", states=_STATES_PROPERTY)
    owners = fields.One2Many('property.owner_boundary',
        'boundary', 'Owners')

    @staticmethod
    def default_type_boundary():
        return 'property_land'

    @classmethod
    def __setup__(cls):
        super(Boundary, cls).__setup__()
        cls._order = [
            ('sequence', 'ASC'),
        ]


class AnnotationLegalTradition(ModelSQL, ModelView):
    'Annotation Legal Tradition'
    __name__ = 'property.annotation_legal_tradition'
    sequence = fields.Char('Sequence', select=True)
    document_number = fields.Char('Document Number', select=True)
    property_study = fields.Many2One('property.study',
            'Property Study', required=True)
    kind = fields.Selection([
            ('tradition', 'Tradition'),
            ('burden', 'Burden'),
            ('cautionary_action', 'Cautionary Action'),
            ('property_title', 'Property Title'),
            ('other', 'Other'),
        ], 'Kind', required=True, select=True)
    kind_string = kind.translated('kind')
    type_property_document = fields.Selection([
            ('auto', 'Auto'),
            ('law', 'Law'),
            ('public_deeds', 'Public Deeds'),
            ('agreement', 'Agreement'),
            ('unavailable', 'Unavailable'),
            ('not_have', 'Not Have'),
            ('official_letter', 'Official Letter'),
            ('resolution', 'Resolution'),
            ('sentence', 'Sentence'),
        ], 'Type Property Document', required=True, select=True)
    type_property_document_string = type_property_document.translated('type_property_document')
    release_property_document = fields.Date('Release Property Document')
    office_property_document = fields.Selection([
            ('notary', 'Notary'),
            ('court', 'Court'),
            ('mayors_office', 'Mayors Office'),
            ('state_agency', 'State Agency'),
        ], 'Office Property Document', select=True)
    office_property_document_string = office_property_document.translated('office_property_document')
    state_agency_note = fields.Char('State Agency Note', states={
            'invisible': Eval('office_property_document') != 'state_agency',
    })
    note_office_property_document = fields.Char('Note Office Property Document',
        select=True)
    city_property_document = fields.Char('City Property Document',
        select=True)
    date_register_property = fields.Date('Date Register Property')
    from_parties = fields.One2Many('property.legal_tradition.from_party', 'legal_tradition_from',
        "From Parties")
    to_parties = fields.One2Many('property.legal_tradition.to_party',
        'legal_tradition_to', "To Parties")
    legal_tradition_comment = fields.Char('Legal Tradition Comment')
    tradition_attribute = fields.Many2One('property.tradition_attribute',
        'Tradition Attribute')
    value = fields.Numeric('Value', digits=(16,2))

    @staticmethod
    def default_value():
        return Decimal(0)


class TraditionAttribute(ModelSQL, ModelView):
    'Tradition Attribute'
    __name__ = 'property.tradition_attribute'
    code = fields.Char('Code', required=True)
    name = fields.Char('Value', required=True)
    description = fields.Char('Description')

    def get_rec_name(self, name):
        if self.code:
            return '[' + self.code + '] ' + self.name
        else:
            return self.name


class PropertyUse(ModelSQL, ModelView):
    'Property Use'
    __name__ = 'property.use'
    name = fields.Char('Name', required=True)
    description = fields.Char('Description')


class PropertyOwner(ModelSQL, ModelView):
    'Property Owner'
    __name__ = 'property.owner'
    property_study = fields.Many2One('property.study', 'Property Study')
    name = fields.Char('Name', required=True)


class PropertyOwnerBoundary(ModelSQL, ModelView):
    'Property Owner Boundary'
    __name__ = 'property.owner_boundary'
    boundary = fields.Many2One('property.boundary', 'Property Boundary')
    name = fields.Char('Name', required=True)

    @staticmethod
    def default_name():
        return "Municipio de Barrancabermeja"


class PropertyOwnerLegal(ModelSQL, ModelView):
    'Property Owner Legal'
    __name__ = 'property.owner_legal'
    property_study = fields.Many2One('property.study', 'Property Study')
    name = fields.Char('Name', required=True)


class LegalTraditionFromParty(ModelSQL, ModelView):
    'Legal Tradition Owner'
    __name__ = 'property.legal_tradition.from_party'
    legal_tradition_from = fields.Many2One('property.annotation_legal_tradition',
            'Legal Tradition From Party')
    name = fields.Char('Name', required=True)


class LegalTraditionToParty(ModelSQL, ModelView):
    'Legal Tradition Owner'
    __name__ = 'property.legal_tradition.to_party'
    legal_tradition_to = fields.Many2One('property.annotation_legal_tradition',
            'Legal Tradition To Party')
    name = fields.Char('Name', required=True)


class PropertyStudyReport(Report):
    'Property Study'
    __name__ = 'property.study'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PropertyStudyReport, cls).get_context(records, data)
        Company = Pool().get('company.company')
        report_context['company'] = Company(Transaction().context.get('company'))
        return report_context


class PropertyTableReport(Report):
    'Property Table Report'
    __name__ = 'property.study_table_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PropertyTableReport, cls).get_context(records, data)
        Study = Pool().get('property.study')
        report_context['records'] = Study.search([])
        return report_context

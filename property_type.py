#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class PropertyType(ModelSQL, ModelView):
    "Property Type"
    __name__ = "property.type"
    name = fields.Char('Name', required=True)
    description = fields.Char('Description')
